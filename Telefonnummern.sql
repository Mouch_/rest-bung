create database Telefonnummern;

create Table Telefonbuch (id int PRIMARY KEY,Telefonnummer varchar(11),Name varchar(45) NOT NULL);

INSERT INTO Telefonbuch 
VALUES	(1,'55555555555', 'Max_Mustermann'),
		(2,'06601827362', 'Johann_Schüsselmann'),
        (3,'83749273947', 'Martin_Martinfrau'),
        (4,'16161616161', 'Günther_Kübelheber');