package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestübungApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestübungApplication.class, args);
    }
}
