package demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel = "Telefonbuch", path = "Telefonbuch")
public interface TelefonbuchRepository extends CrudRepository<Telefonbuch, Integer> {
	
}
