package demo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name="Telefonbuch")
@Entity
public class Telefonbuch  implements Serializable{
	private static final long serialVersionUID = 1L;

	//private static final long serialVersionUID = 7673169434704406106L;

	@Id
	@GeneratedValue//(strategy=GenerationType.AUTO)
	@JsonIgnore
	Integer id;
	
	String Telefonnummer;

	String Name;
	
	public Telefonbuch(){
		
	}
	

	public Telefonbuch(String Telefonnummer, String Name) {
		this.Telefonnummer=Telefonnummer;
		this.Name=Name;
	}

	public Integer getId() {
		return this.id;
	}
	public void setId(Integer id) {
		this.id=id;
	}

	public String getTelefonnummer() {
		return this.Telefonnummer;
	}

	public void setTelefonnummer(String Telefonnummer) {
		this.Telefonnummer = Telefonnummer;
	}

	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}
	
	
	

}
