package demo;

import java.util.Collection;
import java.util.Iterator;

import org.neo4j.cypher.internal.compiler.v2_1.planner.logical.steps.idSeekLeafPlanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Telefonbuch")
public class TelefonnumerController 
{
	@Autowired
	private TelefonbuchRepository telfonnummernRep; 
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public Collection<Telefonbuch> getAll(){
		return (Collection<Telefonbuch>) telfonnummernRep.findAll();
		
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Telefonbuch getTelefonbuch(@PathVariable int id)
	{
		return telfonnummernRep.findOne(id);
	}
	
	@RequestMapping(value="/",method=RequestMethod.POST)
	public ResponseEntity<Telefonbuch> createTelefonbuch(@RequestBody Telefonbuch t)//@RequestParam(value="nummer", required=true) String nummer ,@RequestParam(value="name", required=true) String name)
	{
		//Telefonbuch newTelefonnummer = new Telefonbuch(nummer, name);
		
		telfonnummernRep.save(t);
		return new ResponseEntity<Telefonbuch>(t,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public Telefonbuch updateTelefonbuch(@PathVariable int id, @RequestParam(value="number", required=true) String nummer ,@RequestParam(value="name", required=true) String name)
	{
		Telefonbuch newTelefonnummer = telfonnummernRep.findOne(id);
		newTelefonnummer.setTelefonnummer(nummer);
		newTelefonnummer.setName(name);
		telfonnummernRep.save(newTelefonnummer);
		return newTelefonnummer;
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void deleteTelefonbuch(@PathVariable int id)
	{
		telfonnummernRep.delete(id);
	}
	
	
}
